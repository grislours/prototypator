<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="14"/>
        <source>Prototypator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="27"/>
        <source>C++ code:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="50"/>
        <source>&amp;Run</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="53"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="88"/>
        <source>Compilation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="153"/>
        <source>Input/output</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="56"/>
        <source>testing presence of cmake...
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="61"/>
        <source>running &apos;cmake&apos;...
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="66"/>
        <source>running &apos;make&apos;...
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="71"/>
        <source>running application...
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="124"/>
        <source> exited with code </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="210"/>
        <source>error(s):
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="215"/>
        <source>warning(s):
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="238"/>
        <source>failed to start &apos;</source>
        <translation></translation>
    </message>
</context>
</TS>
