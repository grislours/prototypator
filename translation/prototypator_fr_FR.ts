<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="14"/>
        <source>Prototypator</source>
        <translation>Prototypator</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="27"/>
        <source>C++ code:</source>
        <translation>Code c++ :</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="50"/>
        <source>&amp;Run</source>
        <translation>&amp;Démarrer</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="53"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="88"/>
        <source>Compilation</source>
        <translation>Compilation</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.ui" line="153"/>
        <source>Input/output</source>
        <translation>Entrée/sortie</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="56"/>
        <source>testing presence of cmake...
</source>
        <translation>test de la présence de cmake...
</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="61"/>
        <source>running &apos;cmake&apos;...
</source>
        <translation>démarrage de &apos;cmake&apos;...
</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="66"/>
        <source>running &apos;make&apos;...
</source>
        <translation>démarrage de &apos;make&apos;...
</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="71"/>
        <source>running application...
</source>
        <translation>démarrage de l&apos;application...
</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="124"/>
        <source> exited with code </source>
        <translation> s&apos;est terminé avec le code </translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="210"/>
        <source>error(s):
</source>
        <translation>erreur(s) :
</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="215"/>
        <source>warning(s):
</source>
        <translation>avertissement(s) :
</translation>
    </message>
    <message>
        <location filename="../src/prototypator/mainwindow.cpp" line="238"/>
        <source>failed to start &apos;</source>
        <translation>impossible de démarrer &apos;</translation>
    </message>
</context>
</TS>
