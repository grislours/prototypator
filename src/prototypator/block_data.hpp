
#ifndef BLOCK_DATA_HPP
#define BLOCK_DATA_HPP

#include <QTextBlockUserData>

class BlockData
  : public QTextBlockUserData
{
public:
  std::string msg = {};
  bool has_errors = false;

public:
  BlockData( std::string msg, bool has_errors );
  ~BlockData() override;
};

#endif
