
#include "mainwindow.hpp"
#include "utils/qt/translator.hpp"
#include <QApplication>
#include <QLibraryInfo>

int main( int argc, char **argv ) {
  // global settings
  QCoreApplication::setOrganizationName( "gamasoft" );
  QCoreApplication::setOrganizationDomain( "gamasoft.fr" );
  QCoreApplication::setApplicationName( "prototypator" );

  // start application
  auto app = QApplication{ argc, argv };
  auto qt_translator = qt::Translator{ app, "qt_", QLibraryInfo::location( QLibraryInfo::TranslationsPath ) };
  auto app_translator = qt::Translator{ app, "prototypator_" };
  auto mainWin = MainWindow{};
  mainWin.show();
  return QApplication::exec();
}
