
#ifndef SYNTAX_HIGHLIGHTER_HPP
#define SYNTAX_HIGHLIGHTER_HPP

#include <QSyntaxHighlighter>
#include <regex>
#include <vector>

class SyntaxHighlighter
  : public QSyntaxHighlighter
{
  Q_OBJECT

public:
  explicit SyntaxHighlighter( QTextDocument *parent = nullptr );

protected:
  void highlightBlock( const QString &text ) override;

private:
  QTextCharFormat format_error_;
  QTextCharFormat format_warning_;
  QTextCharFormat format_multiline_comment_;
  std::regex regex_comment_start_;
  std::regex regex_comment_end_;
  std::vector<std::pair<std::regex, QTextCharFormat>> regex_and_formats_;
};

#endif
