#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "syntax_highlighter.hpp"
#include <QMainWindow>
#include <QProcess>
#include <experimental/memory>
#include <filesystem>
#include <memory>

// forward declarations
namespace Ui {
class MainWindow;
}
class QPlainTextEdit;

struct Command {
  std::string introduction = "";
  std::string program = "";
  std::vector<std::string> arguments = {};
};

class MainWindow
  : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow( QWidget *parent = nullptr );
  ~MainWindow() override;

public slots:
  void finished( int exit_code );
  void run();
  void update_std_err();
  void update_std_out();

protected:
  void closeEvent( QCloseEvent *event_ptr ) override;
  bool eventFilter( QObject *obj_ptr, QEvent *event_ptr ) override;

private:
  void append_text( const std::string &text, const QColor &color );
  void disable();
  void enable();
  void execute_next_command();
  void prepare_directory();
  void restore_settings();
  void update_blocks();

private:
  struct Messages {
    std::vector<std::string> warnings = {};
    std::vector<std::string> errors = {};
  };

private:
  std::unique_ptr<Ui::MainWindow> ui_ptr_;
  bool is_prepared_ = false;
  std::filesystem::path directory_;
  QProcess process_;
  std::experimental::observer_ptr<QPlainTextEdit> plain_text_;
  std::vector<Command> commands_;
  unsigned current_command_index_ = 0;
  std::map<int, Messages> line_and_messages_;
  std::unique_ptr<SyntaxHighlighter> syntax_highlighter_ptr_;
};

#endif // MAINWINDOW_HPP
