
#include "block_data.hpp"

BlockData::BlockData( std::string msg, bool has_errors )
  : msg{ std::move( msg ) }
  , has_errors{ has_errors }
{
  // null
}

BlockData::~BlockData() = default;
