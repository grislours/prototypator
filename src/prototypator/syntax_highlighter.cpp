
#include "block_data.hpp"
#include "syntax_highlighter.hpp"
#include <QFont>
#include <QTextDocument>
#include <memory>
#include <sstream>

namespace {

const auto keywords = {
  "alignas", "alignof", "and", "and_eq", "asm", "atomic_cancel", "atomic_commit", "atomic_noexcept",
  "auto", "bitand", "bitor", "bool", "break", "case", "catch", "char", "char8_t", "char16_t",
  "char32_t", "class", "compl", "concept", "const", "consteval", "constexpr", "const_cast", "continue",
  "co_await", "co_return", "co_yield", "decltype", "default", "delete", "do", "double", "dynamic_cast",
  "else", "enum", "explicit", "export", "extern", "false", "float", "for", "friend", "goto", "if",
  "inline", "int", "long", "mutable", "namespace", "new", "noexcept", "not", "not_eq", "nullptr",
  "operator", "or", "or_eq", "private", "protected", "public", "reflexpr", "register", "reinterpret_cast",
  "requires", "return", "short", "signed", "sizeof", "static", "static_assert", "static_cast", "struct",
  "switch", "synchronized", "template", "this", "thread_local", "throw", "true", "try", "typedef",
  "typeid", "typename", "union", "unsigned", "using", "virtual", "void", "volatile", "wchar_t", "while",
  "xor", "xor_eq",
};

const std::vector<std::pair<const char *, QColor>> regex_and_colors = {
  { "^#.*", Qt::darkYellow }, // preprocessor
  { "\\bstd::[a-z0-9]+\\b", Qt::blue }, // standard types
  { "\\b[A-Z][a-z0-9_]+", Qt::blue }, // types
  { "(\".*\")|(\\b\\d+\\b)", Qt::darkGreen }, // string literals
  { "//[^\n]*", Qt::gray }, // comment
};

}

SyntaxHighlighter::SyntaxHighlighter( QTextDocument *parent )
  : QSyntaxHighlighter( parent )
  , format_error_{}
  , format_warning_{}
  , format_multiline_comment_{}
  , regex_comment_start_{ R"(/\*)" }
  , regex_comment_end_{ R"(\*/)" }
  , regex_and_formats_{}
{
  // formats
  format_error_.setUnderlineStyle( QTextCharFormat::WaveUnderline );
  format_error_.setUnderlineColor( Qt::red );
  format_warning_.setUnderlineStyle( QTextCharFormat::WaveUnderline );
  format_warning_.setUnderlineColor( Qt::darkYellow );
  format_multiline_comment_.setForeground( Qt::gray );

  // constructs keyword regex
  auto os = std::ostringstream{};
  os << "\\b(";
  auto first = true;
  for( const auto &it : keywords ) {
    if( not first ) {
      os << '|';
    }
    os << it;
    first = false;
  }
  os << ")\\b";
  auto format_keyword = QTextCharFormat{};
  format_keyword.setForeground( Qt::darkMagenta );
  format_keyword.setFontWeight( QFont::Bold );
  regex_and_formats_.emplace_back( std::regex{ os.str() }, format_keyword );

  // other regexs
  for( const auto &[ regex, color ] : regex_and_colors ) {
    auto format = QTextCharFormat{};
    format.setForeground( color );
    regex_and_formats_.emplace_back( std::regex{ regex }, format );
  }
}

void SyntaxHighlighter::highlightBlock( const QString &text )
{
  // prepare format to merge
  auto format_to_merge = QTextCharFormat{};

  // on errors, underline
  auto *data_block_ptr = static_cast<BlockData*>( currentBlock().userData() );
  if( data_block_ptr != nullptr ) {
    auto format = data_block_ptr->has_errors ? format_error_ : format_warning_;
    setFormat( 0, text.length(), format );
    format_to_merge = format;
  }

  // regex
  const auto &text_str = text.toStdString();
  for( const auto &[ regex, format ] : regex_and_formats_ ) {
    auto merged_format = format;
    merged_format.merge( format_to_merge );
    auto begin_it = std::sregex_iterator{ text_str.begin(), text_str.end(), regex };
    for( auto it = begin_it; it != std::sregex_iterator{}; ++it ) {
      auto match = *it;
      setFormat( static_cast<int>( match.position() ), static_cast<int>( match.length() ), merged_format );
    }
  }

  // compute comments zone
  constexpr auto end_line_is_in_comment = 1;
  constexpr auto end_line_is_not_in_comment = 0;

  setCurrentBlockState( end_line_is_not_in_comment );
  auto start_index = 0;
  if( previousBlockState() != end_line_is_in_comment ) {
    auto match = std::smatch{};
    if( std::regex_search( text_str.begin(), text_str.end(), match, regex_comment_start_ ) ) {
      start_index = static_cast<int>( match.position() );
    } else {
      start_index = -1;
    }
  }

  auto format = format_multiline_comment_;
  format.merge( format_to_merge );
  while( start_index >= 0 ) {
    auto match = std::smatch{};
    auto is_matching = std::regex_search( text_str.begin() + start_index, text_str.end(), match, regex_comment_end_ );
    auto comment_length = 0L;
    if( not is_matching ) {
      setCurrentBlockState( end_line_is_in_comment );
      comment_length = static_cast<long>( text_str.length() - static_cast<unsigned>( start_index ) );
    } else {
      auto end_index = start_index + match.position();
      comment_length = end_index - start_index + match.length();
    }
    setFormat( start_index, static_cast<int>( comment_length ), format );
    if( std::regex_search( text_str.begin() + start_index + comment_length, text_str.end(), match, regex_comment_start_ ) ) {
      start_index = static_cast<int>( start_index + comment_length + match.position() );
    } else {
      start_index = -1;
    }
  }
}
