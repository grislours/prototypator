
#include "block_data.hpp"
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "utils/algorithm.hpp"
#include <QSettings>
#include <QStandardPaths>
#include <QToolTip>
#include <QUuid>
#include <fstream>
#include <regex>

namespace fs = std::filesystem;

MainWindow::MainWindow( QWidget *parent )
  : QMainWindow( parent )
  , ui_ptr_{ new Ui::MainWindow }
  , directory_{}
  , process_{ parent }
  , plain_text_{ nullptr }
  , commands_{}
  , line_and_messages_{}
  , syntax_highlighter_ptr_{}
{
  // prepare qt things
  ui_ptr_->setupUi( this );

  // set dock widgets
  setTabPosition( Qt::BottomDockWidgetArea, QTabWidget::West );
  tabifyDockWidget( ui_ptr_->dockWidget_compilation, ui_ptr_->dockWidget_io  );
  ui_ptr_->dockWidget_compilation->setTitleBarWidget( new QWidget{} );
  ui_ptr_->dockWidget_io->setTitleBarWidget( new QWidget{} );

  // set process
  connect( &process_, &QProcess::readyReadStandardOutput, this, &MainWindow::update_std_out );
  connect( &process_, &QProcess::readyReadStandardError, this, &MainWindow::update_std_err );
  connect( &process_, SIGNAL(finished(int)), this, SLOT(finished(int)));

  // event filter for tooltip
  ui_ptr_->textEdit_code->installEventFilter( this );

  // set syntax highlighter
  syntax_highlighter_ptr_ = std::make_unique<SyntaxHighlighter>( ui_ptr_->textEdit_code->document() );

  // restore settings
  restore_settings();

  // prepare context
  prepare_directory();

  // prepare commands
  using namespace std::string_literals;
  const auto build_dir = (directory_ / "build").string();
  commands_ = {
    {
      tr( "testing presence of cmake...\n" ).toStdString(),
      "cmake",
      {"--version"}
    },
    {
      tr( "running 'cmake'...\n" ).toStdString(),
      "cmake",
      {"-G", "Ninja", "-B"s + build_dir, "-H"s + directory_.string()}
    },
    {
      tr( "running 'make'...\n" ).toStdString(),
      "ninja",
      {"-C", build_dir}
    },
    {
      tr( "running application...\n" ).toStdString(),
      build_dir + "/prototypator",
      {}
    },
  };
}

void MainWindow::run()
{
  // preparation
  disable();

  // copy code
  auto fos = std::ofstream{};
  fos.exceptions( std::ifstream::failbit );
  fos.open( directory_ / "main.cpp", std::fstream::trunc );
  fos << ui_ptr_->textEdit_code->toPlainText().toStdString();
  fos.close();

  // go !
  current_command_index_ = is_prepared_ ? 2 : 0;
  execute_next_command();
}

void MainWindow::update_std_out()
{
  append_text( process_.readAllStandardOutput().data(), Qt::black );
}

void MainWindow::update_std_err()
{
  append_text( process_.readAllStandardError().data(), Qt::darkRed );
}

void MainWindow::finished( int exit_code )
{
  // execute next command
  ++current_command_index_;

  // check if prepation or compilation is finished
  if( exit_code == 0 ) {
    if( current_command_index_ == 2 ) {
      is_prepared_ = true;
    } else if( current_command_index_ == 3 ) {
      plain_text_.reset( ui_ptr_->plainTextEdit_io );
      ui_ptr_->dockWidget_io->raise();
    }
  }

  // if failed or everything finished
  if( exit_code != 0 or current_command_index_ == commands_.size() ) {
    auto os = std::ostringstream{};
    os << process_.program().toStdString()
       << tr( " exited with code " ).toStdString() << exit_code;
    append_text( os.str(), exit_code == 0 ? Qt::darkRed : Qt::darkGreen );

    // enable
    enable();

    return;
  }

  // go next
  execute_next_command();
}

MainWindow::~MainWindow() = default;

void MainWindow::closeEvent( QCloseEvent *event_ptr )
{
  // delete directory
  fs::remove_all( directory_ );

  // save settings
  auto settings = QSettings{};
  settings.beginGroup( "main_window" );
  settings.setValue( "geometry", saveGeometry() );
  settings.setValue( "windowState", saveState() );
  settings.setValue( "code", ui_ptr_->textEdit_code->toPlainText() );
  settings.endGroup();
  QMainWindow::closeEvent( event_ptr );
}

void MainWindow::prepare_directory()
{
  // get unique writeable directory
  auto cache_dir = QStandardPaths::writableLocation( QStandardPaths::CacheLocation );
  directory_ = (cache_dir + "/" + QUuid::createUuid().toString()).toStdString();

  // create directory
  fs::create_directories( directory_ / "build" );

  // copy files
  QFile::copy( ":/resources/CMakeLists.txt", QString::fromStdString( directory_ / "CMakeLists.txt" ) );
  QFile::copy( ":/resources/main.cpp", QString::fromStdString( directory_ / "main.cpp" ) );
  fs::permissions( directory_ / "main.cpp", fs::perms::owner_write, fs::perm_options::add );
}

void MainWindow::restore_settings()
{
  auto settings = QSettings{};
  settings.beginGroup( "main_window" );
  restoreGeometry( settings.value( "geometry" ).toByteArray() );
  restoreState( settings.value( "windowState" ).toByteArray() );
  ui_ptr_->textEdit_code->setText( settings.value( "code" ).toString() );
  settings.endGroup();
}

bool MainWindow::eventFilter( QObject *obj_ptr, QEvent *event_ptr )
{
  if( obj_ptr == ui_ptr_->textEdit_code and event_ptr->type() == QEvent::ToolTip ) {
    auto *help_event_ptr = static_cast<QHelpEvent *>( event_ptr );
    auto cursor = ui_ptr_->textEdit_code->cursorForPosition( help_event_ptr->pos() );
    cursor.select( QTextCursor::LineUnderCursor );

    // get msg
    auto *block_data = static_cast<BlockData*>( cursor.block().userData() );
    if( block_data == nullptr ) {
      QToolTip::hideText();
    } else {
      QToolTip::showText( help_event_ptr->globalPos(), QString::fromStdString( block_data->msg ) );
    }
    return true;
  }
  return false;
}

void MainWindow::update_blocks()
{
  auto &document = *ui_ptr_->textEdit_code->document();
  for( auto i = 0; i < document.blockCount(); ++i ) {
    auto it = line_and_messages_.find( i + 1 );
    auto block = document.findBlockByNumber( i );
    if( it != line_and_messages_.end() ) {
      auto result = std::string{ "" };
      auto has_errors = false;
      const auto &errors = it->second.errors;
      if( not errors.empty() ) {
        has_errors = true;
        result = tr( "error(s):\n" ).toStdString() + utils::join( errors.begin(), errors.end(), "\n" );
      }
      const auto &warnings = it->second.warnings;
      if( not warnings.empty() ) {
        if( has_errors ) result += '\n';
        result += tr( "warning(s):\n" ).toStdString() + utils::join( warnings.begin(), warnings.end(), "\n" );
      }

      // store message into the corresponding block
      auto block_data_ptr = std::make_unique<BlockData>( result, has_errors );
      block.setUserData( block_data_ptr.release() );
    } else {
      block.setUserData( nullptr );
    }
  }
}

void MainWindow::execute_next_command()
{
  const auto &command = commands_.at( current_command_index_ );
  append_text( command.introduction, Qt::darkGreen );
  auto arguments = QStringList{};
  for( const auto &argument : command.arguments ) {
    arguments.push_back(QString::fromStdString(argument));
  }
  process_.start( QString::fromStdString( command.program ), arguments );
  auto started = process_.waitForStarted();
  if( not started ) {
    const auto msg = tr( "failed to start '" ) + process_.program() + '\'';
    append_text( msg.toStdString(), Qt::darkRed );
    enable();
  }
}

void MainWindow::disable()
{
  QApplication::setOverrideCursor( Qt::WaitCursor );
  setEnabled( false );
  ui_ptr_->dockWidget_compilation->raise();
  ui_ptr_->plainTextEdit_compilation->clear();
  ui_ptr_->plainTextEdit_io->clear();
  plain_text_.reset( ui_ptr_->plainTextEdit_compilation );
  line_and_messages_.clear();
  update_blocks();
  syntax_highlighter_ptr_->rehighlight();
}

void MainWindow::enable()
{
  QApplication::restoreOverrideCursor();
  setEnabled( true );
  update_blocks();
  syntax_highlighter_ptr_->rehighlight();
}

void MainWindow::append_text( const std::string &text, const QColor &color )
{
  // insert text
  auto cursor = plain_text_->textCursor();
  cursor.movePosition( QTextCursor::End );
  auto format = QTextCharFormat{};
  format.setForeground( QBrush{ color } );
  cursor.insertText( text.data(), format );
  plain_text_->moveCursor( QTextCursor::End );

  // analyse and save errors or warnings
  static const std::regex error_regex{ R"(\.\.\/main\.cpp\:(\d+)\:\d+: (error|warning)\: (.*))" };
  auto begin_it = std::sregex_iterator( text.begin(), text.end(), error_regex );
  for( auto it = begin_it; it != std::sregex_iterator{}; ++it ) {
    auto match = *it;
    if( match.str( 2 ) == "error" ) {
      line_and_messages_[std::stoi( match.str( 1 ) )].errors.push_back( match.str( 3 ) );
    } else {
      line_and_messages_[std::stoi( match.str( 1 ) )].warnings.push_back( match.str( 3 ) );
    }
  }
}
