
#ifndef UTILS_EXCEPTION_HPP
#define UTILS_EXCEPTION_HPP

#include <iostream>

namespace utils {

template<typename Lambda_>
void ignore_exceptions( const Lambda_ &lambda ) {
  try {
    lambda();
  } catch( const std::exception &e ) {
    try {
      std::cerr << std::string{ e.what()  } + '\n';
    } catch( ... ) {
      // ignore
    }
  } catch( ... ) {
    try {
      std::cerr << "unknown error\n";
    } catch( ... ) {
      // ignore
    }
  }
}

}

#endif
