
#ifndef UTILS_OSTREAM_HPP
#define UTILS_OSTREAM_HPP

#include <array>
#include <iostream>
#include <map>
#include <set>
#include <tuple>
#include <vector>

namespace utils {

template<typename First_, typename Second_>
std::ostream &operator<<(
  std::ostream &os,
  const std::pair<First_, Second_> &pair
) {
  os << pair.first << " : " << pair.second;
  return os;
}

template<typename Iterator_>
std::ostream &operator<<(
  std::ostream &os,
  const std::tuple<Iterator_, Iterator_, const char *> &begin_end_separator
) {
  os << '(';
  bool is_first = true;
  for(
    Iterator_ it = std::get<0>( begin_end_separator );
    it != std::get<1>( begin_end_separator );
    ++it
  ) {
    if( not is_first ) {
      os << std::get<2>( begin_end_separator );
    }
    os << *it;
    is_first = false;
  }
  os << ')';
  return os;
}

template<typename Type_>
std::ostream &operator<<( std::ostream &os, const std::set<Type_> &container ) {
  return operator<<( os, std::make_tuple( container.begin(), container.end(), ", " ) );
}

template<typename Type_>
std::ostream &operator<<( std::ostream &os, const std::vector<Type_> &container ) {
  return operator<<( os, std::make_tuple( container.begin(), container.end(), ", " ) );
}

template<typename Key_, typename Elt_>
std::ostream &operator<<( std::ostream &os, const std::map<Key_, Elt_> &container ) {
  return operator<<( os, std::make_tuple( container.begin(), container.end(), "; " ) );
}

}

#endif
