#ifndef UTILS_ALGORITHM_HPP
#define UTILS_ALGORITHM_HPP

namespace utils {

template<typename Iterator_>
auto join( Iterator_ begin, Iterator_ end, typename Iterator_::value_type separator )
-> typename Iterator_::value_type
{
  typename Iterator_::value_type result = {};
  auto first = true;
  for( auto it = begin; it != end; ++it ) {
    const auto &elt = *it;
    if( not first ) result += separator;
    result += elt;
    first = false;
  }
  return result;
}

}

#endif
