cmake_minimum_required( VERSION 3.6 )

project( prototypator )

set( CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}" )

# translation files
set( ${PROJECT_NAME}_TS
    en_GB
    fr_FR
)
list( TRANSFORM ${PROJECT_NAME}_TS PREPEND "${CMAKE_CURRENT_SOURCE_DIR}/translation/${PROJECT_NAME}_" )
list( TRANSFORM ${PROJECT_NAME}_TS APPEND ".ts" )

# compilation
set( CMAKE_CXX_STANDARD 17 )
set( CXX_STANDARD_REQUIRED ON )

if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
  add_compile_options(-Wall -Wextra -pedantic -Werror -Weffc++)
endif()

add_subdirectory(src)
